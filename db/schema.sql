-- -----------------------------------------------------
-- Database Schema angular_app_db
-- -----------------------------------------------------

CREATE SCHEMA IF NOT EXISTS `angular_app_db` DEFAULT CHARACTER SET utf8mb4 ;
USE `angular_app_db` ;

-- -----------------------------------------------------
-- Table `user_tb`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `user_tb` ;

CREATE TABLE IF NOT EXISTS `user_tb` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `user_name` VARCHAR(15) NOT NULL,
  `password` VARCHAR(256) NOT NULL,
  `insert_date` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` TIMESTAMP NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8;

-- -----------------------------------------------------
-- Table `post_tb`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `post_tb` ;

CREATE TABLE IF NOT EXISTS `post_tb` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(256) NOT NULL,
  `text` TEXT NOT NULL,
  `insert_date` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` TIMESTAMP NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8;
