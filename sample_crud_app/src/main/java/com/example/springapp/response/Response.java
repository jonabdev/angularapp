package com.example.springapp.response;

public class Response<T> {
    private T item;
    private int code;
    private String message;

    public Response() {}

    private Response(Builder builder) {
        setItem((T) builder.item);
        setCode(builder.code);
        setMessage(builder.message);
    }

    public T getItem() {
        return item;
    }

    public void setItem(T item) {
        this.item = item;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "Response{" +
                "item=" + item +
                ", code=" + code +
                ", message='" + message + '\'' +
                '}';
    }

    public static final class Builder<T> {
        private T item;
        private int code;
        private String message;

        public Builder() {
        }

        public Builder item(T val) {
            item = val;
            return this;
        }

        public Builder code(int val) {
            code = val;
            return this;
        }

        public Builder message(String val) {
            message = val;
            return this;
        }

        public Response build() {
            return new Response(this);
        }
    }
}
