package com.example.springapp.model;

import java.util.Calendar;

import javax.persistence.*;

@Entity
@Table(name="post_tb")
public class Post {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "title", nullable = false)
    private String title;

    @Lob
    @Column(name = "text", nullable = false)
    private String text;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "insert_date", nullable = false)
    private Calendar insertDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "update_date")
    private Calendar updateDate;

    public Post() {
    }

    public Post(String title, String text, Calendar insertDate) {
        this.title = title;
        this.text = text;
        this.insertDate = insertDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Calendar getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(Calendar insertDate) {
        this.insertDate = insertDate;
    }

    public Calendar getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Calendar updateDate) {
        this.updateDate = updateDate;
    }

    @Override
    public String toString() {
        return "Post{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", text='" + text + '\'' +
                ", insertDate=" + insertDate +
                ", updateDate=" + updateDate +
                '}';
    }
}
