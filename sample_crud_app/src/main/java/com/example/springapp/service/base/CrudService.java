package com.example.springapp.service.base;

import com.example.springapp.exception.ServiceException;

import java.util.List;
import java.util.Optional;

public interface CrudService<T> {

    T save(T entity) throws ServiceException;

    void delete(T entity) throws ServiceException;

    Optional<T> findById(Long id) throws ServiceException;

    List<T> findAll() throws ServiceException;

}
