package com.example.springapp.service.post;

import com.example.springapp.exception.ServiceException;
import com.example.springapp.model.Post;
import com.example.springapp.service.base.CrudService;

import java.util.Optional;

public interface PostService extends CrudService<Post> {

    Optional<Post> getFirstPost() throws ServiceException;

    void deleteById(Long id) throws ServiceException;
}
