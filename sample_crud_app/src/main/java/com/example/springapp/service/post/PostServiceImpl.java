package com.example.springapp.service.post;

import com.example.springapp.exception.ServiceException;
import com.example.springapp.model.Post;
import com.example.springapp.repository.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional(propagation = Propagation.REQUIRED)
public class PostServiceImpl implements PostService {

    private final PostRepository postRepository;

    @Autowired
    public PostServiceImpl(PostRepository postRepository) {
        this.postRepository = postRepository;
    }

    @Override
    public Post save(Post entity) throws ServiceException {
        return postRepository.save(entity);
    }


    @Override
    public void delete(Post entity) throws ServiceException {
        postRepository.delete(entity);
    }

    @Override
    public Optional<Post> findById(Long id) throws ServiceException {
        return postRepository.findById(id);
    }

    @Override
    public List<Post> findAll() throws ServiceException {
        return postRepository.findAll();
    }

    @Override
    public Optional<Post> getFirstPost() throws ServiceException {
        return postRepository.findFirstByIdIsNotNull();
    }

    @Override
    public void deleteById(Long id) throws ServiceException {
        postRepository.deleteById(id);
    }

    public PostRepository getPostRepository() {
        return postRepository;
    }
}