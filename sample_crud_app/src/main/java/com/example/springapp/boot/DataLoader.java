package com.example.springapp.boot;

import com.example.springapp.helper.LogHelper;
import com.example.springapp.model.Post;
import com.example.springapp.service.post.PostService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.Calendar;


@Component
public class DataLoader implements CommandLineRunner {

    private static Logger logger = LogManager.getLogger(DataLoader.class);

    private PostService postService;

    @Autowired
    public DataLoader(PostService postService) {
        this.postService = postService;
    }

    @Override
    public void run(String... strings) {
        try {
            if (!getPostService().getFirstPost().isPresent()) {
                Post post1 = new Post();
                post1.setTitle("Test 1");
                post1.setText("This is the first post! Lorem ipsum dolor sit amet, consectetur adipiscing elit. " +
                        "Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi. " +
                        "Nulla quis sem at nibh elementum imperdiet. Duis sagittis ipsum. Praesent mauris. " +
                        "Fusce nec tellus sed augue semper porta. Mauris massa. Vestibulum lacinia arcu eget nulla. " +
                        "Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. " +
                        "Curabitur sodales ligula in libero. Sed dignissim lacinia nunc. ");
                post1.setInsertDate(Calendar.getInstance());
                getPostService().save(post1);

                Post post2 = new Post();
                post2.setTitle("Test 2");
                post2.setText("This is the second post! Curabitur tortor. Pellentesque nibh. Aenean quam. " +
                        "In scelerisque sem at dolor. Maecenas mattis. Sed convallis tristique sem. " +
                        "Proin ut ligula vel nunc egestas porttitor. Morbi lectus risus, iaculis vel, suscipit quis, " +
                        "luctus non, massa. Fusce ac turpis quis ligula lacinia aliquet. Mauris ipsum. Nulla metus metus, " +
                        "ullamcorper vel, tincidunt sed, euismod in, nibh. Quisque volutpat condimentum velit. " +
                        "Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. " +
                        "Nam nec ante. ");
                post2.setInsertDate(Calendar.getInstance());
                getPostService().save(post2);
            }
        } catch (Exception ex) {
            LogHelper.logError(logger, "run", ex);
        }
    }

    private PostService getPostService() {
        return postService;
    }
}
