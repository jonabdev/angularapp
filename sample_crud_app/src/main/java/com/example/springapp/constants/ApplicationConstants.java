package com.example.springapp.constants;

public class ApplicationConstants {

    public static int OPERATION_CODE_SUCCESS = 0;
    public static int OPERATION_CODE_ERROR = 1;
    public static int OPERATION_CODE_WARNING = 2;

    public static final String GENERIC_ERROR = "An error occurred while processing your request, please try again";
    public static final String SECURITY_GENERIC_ERROR = "User credentials not found, please try again";
}
