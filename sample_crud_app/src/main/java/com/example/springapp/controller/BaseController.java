package com.example.springapp.controller;

import com.example.springapp.constants.ApplicationConstants;
import com.example.springapp.helper.LogHelper;
import com.example.springapp.response.Response;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;


public class BaseController {

    public <T> ResponseEntity<Response<T>> prepareResponseEntityWithData(int code, String message, T item, HttpStatus httpStatus) {
        Response response = new Response();

        response.setCode(code);
        response.setMessage(message);
        response.setItem(item);

        return new ResponseEntity(response, httpStatus);
    }

    public <T> ResponseEntity<Response<T>> logAndPrepareResponseEntityForException(Logger logger, String method, Exception ex) {
        LogHelper.logError(logger, method, ex);

        Response response = new Response();
        response.setCode(ApplicationConstants.OPERATION_CODE_ERROR);
        if (StringUtils.isEmpty(ex.getMessage())) {
            response.setMessage(ApplicationConstants.GENERIC_ERROR);
        } else {
            response.setMessage(ex.getMessage());
        }
        return new ResponseEntity(response, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
