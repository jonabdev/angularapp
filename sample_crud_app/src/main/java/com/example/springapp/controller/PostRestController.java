package com.example.springapp.controller;

import com.example.springapp.constants.ApplicationConstants;
import com.example.springapp.helper.LogHelper;
import com.example.springapp.model.Post;
import com.example.springapp.response.Response;
import com.example.springapp.service.post.PostService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.Calendar;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/ddd")
public class PostRestController extends BaseController {

    private static Logger logger = LogManager.getLogger(PostRestController.class);

    private final PostService postService;

    @Autowired
    public PostRestController(PostService postService) {
        this.postService = postService;
    }

    @GetMapping("/posts")
    public ResponseEntity<Response<List<Post>>> getAllPosts() {
        LogHelper.logInfo(logger, "getAllPosts", "Get all posts...");

        try {
            List<Post> posts = getPostService().findAll();
            return prepareResponseEntityWithData(ApplicationConstants.OPERATION_CODE_SUCCESS,
                    "Posts are loaded successfully.",
                    posts,
                    HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return logAndPrepareResponseEntityForException(logger, "getAllPosts", e);
        }
    }

    @GetMapping(value = "posts/{id}")
    public ResponseEntity<Response<Post>> getPost(@PathVariable String id) {
        LogHelper.logInfo(logger, "getPost", "Get Post with ID = " + id);

        try {
            if (StringUtils.isEmpty(id)) {
                return prepareResponseEntityWithData(ApplicationConstants.OPERATION_CODE_WARNING,
                        "Please enter post id before proceeding.",
                        null,
                        HttpStatus.OK);
            }
            Optional<Post> post = getPostService().findById(Long.valueOf(id));
            return post.map(post1 -> prepareResponseEntityWithData(ApplicationConstants.OPERATION_CODE_SUCCESS,
                    "Post is found!",
                    post1,
                    HttpStatus.OK)).orElseGet(() -> prepareResponseEntityWithData(ApplicationConstants.OPERATION_CODE_ERROR,
                    "Post doesn't exist!",
                    null,
                    HttpStatus.OK));
        } catch (NumberFormatException e) {
            return prepareResponseEntityWithData(ApplicationConstants.OPERATION_CODE_WARNING,
                    "Please enter a valid post id before proceeding.",
                    null,
                    HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return logAndPrepareResponseEntityForException(logger, "getPost", e);
        }
    }

    @PostMapping("/posts")
    public ResponseEntity<Response<Post>> createNewPost(@RequestBody Post post) {
        LogHelper.logInfo(logger, "createNewPost", "Create new post...");

        try {
            if (StringUtils.isEmpty(post.getTitle()) || StringUtils.isEmpty(post.getText())) {
                return prepareResponseEntityWithData(ApplicationConstants.OPERATION_CODE_WARNING,
                        "Please enter a title and content for your post before proceeding.",
                        null,
                        HttpStatus.OK);
            }
            Post postSaved = getPostService().save(new Post(post.getTitle(), post.getText(), Calendar.getInstance()));
            return prepareResponseEntityWithData(ApplicationConstants.OPERATION_CODE_SUCCESS,
                    "Post created successfully!",
                    postSaved,
                    HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return logAndPrepareResponseEntityForException(logger, "createNewPost", e);
        }
    }

    @PutMapping("/posts")
    public ResponseEntity<Response<Post>> updatePost(@RequestBody Post post) {
        LogHelper.logInfo(logger, "updatePost", "Update Post with ID = " + post.getId());

        try {
            if (StringUtils.isEmpty(post.getTitle()) || StringUtils.isEmpty(post.getText())) {
                return prepareResponseEntityWithData(ApplicationConstants.OPERATION_CODE_WARNING,
                        "Please enter a title and content for your post before proceeding.",
                        null,
                        HttpStatus.OK);
            }
            Optional<Post> postData = getPostService().findById(post.getId());
            if (postData.isPresent()) {
                Post postToUpdate = postData.get();
                postToUpdate.setTitle(post.getTitle());
                postToUpdate.setText(post.getText());
                postToUpdate.setUpdateDate(Calendar.getInstance());

                Post postSaved = getPostService().save(postToUpdate);
                return prepareResponseEntityWithData(ApplicationConstants.OPERATION_CODE_SUCCESS,
                        "Post updated successfully!",
                        postSaved,
                        HttpStatus.OK);
            } else {
                return prepareResponseEntityWithData(ApplicationConstants.OPERATION_CODE_ERROR,
                        "Post doesn't exist!",
                        null,
                        HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return logAndPrepareResponseEntityForException(logger, "updatePost", e);
        }
    }


    @DeleteMapping("/posts/{id}")
    public ResponseEntity<Response<String>> deletePost(@PathVariable("id") Long id) {
        LogHelper.logInfo(logger, "deletePost", "Delete Post with ID = " + id);

        try {
            getPostService().deleteById(id);
            return prepareResponseEntityWithData(ApplicationConstants.OPERATION_CODE_SUCCESS,
                    "Post has been deleted.",
                    "Post deleted successfully!",
                    HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return logAndPrepareResponseEntityForException(logger, "deletePost", e);
        }
    }

    private PostService getPostService() {
        return postService;
    }
}

