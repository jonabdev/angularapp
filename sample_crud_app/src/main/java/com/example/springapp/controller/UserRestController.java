package com.example.springapp.controller;

import com.example.springapp.constants.ApplicationConstants;
import com.example.springapp.helper.LogHelper;
import com.example.springapp.model.User;
import com.example.springapp.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.Base64;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class UserRestController extends BaseController {
    private static Logger logger = LogManager.getLogger(UserRestController.class);

    @PostMapping("/login")
    public ResponseEntity<Response<Boolean>> login(@RequestBody User user) {
        LogHelper.logInfo(logger, "login", "Login...");
        try {
            if (StringUtils.isEmpty(user.getUsername()) || StringUtils.isEmpty(user.getPassword())) {
                return prepareResponseEntityWithData(ApplicationConstants.OPERATION_CODE_WARNING,
                        "Please enter username and password before proceeding.",
                        Boolean.FALSE,
                        HttpStatus.OK);
            }
            if (user.getUsername().equalsIgnoreCase("admin") && user.getPassword().equals("admin")) {
                return prepareResponseEntityWithData(ApplicationConstants.OPERATION_CODE_SUCCESS,
                        "Login was successful!.",
                        Boolean.TRUE,
                        HttpStatus.OK);
            } else {
                return prepareResponseEntityWithData(ApplicationConstants.OPERATION_CODE_ERROR,
                        ApplicationConstants.SECURITY_GENERIC_ERROR,
                        Boolean.FALSE,
                        HttpStatus.OK);
            }
        } catch (Exception e) {
            return logAndPrepareResponseEntityForException(logger, "login", e);
        }
    }

    @PostMapping("/user")
    public Principal getUser(HttpServletRequest request) {
        LogHelper.logInfo(logger, "getUser", "Get authenticated user...");

        String authToken = request.getHeader("Authorization")
                .substring("Basic".length()).trim();
        return () -> new String(Base64.getDecoder().decode(authToken)).split(":")[0];
    }
}
