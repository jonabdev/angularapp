package com.example.springapp.helper;

import com.example.springapp.constants.ApplicationConstants;
import org.apache.logging.log4j.Logger;

public class LogHelper {

    public static void logInfo(Logger logger, String operation, String message) {
        logger.info(operation + " - " + message);
    }

    public static void logError(Logger logger, String operation, Exception e) {
        String message = ApplicationConstants.GENERIC_ERROR;
        if (e == null) {
            logger.error(operation + " - " + message);
            return;
        }
        logger.error(operation + " - " + message + " ERROR: ", e.getMessage());
        if (e.getCause() != null) {
            logger.error("CAUSE 1: " + e.getCause().getMessage());

            if (e.getCause().getCause() != null) {
                logger.error("CAUSE 2: " + e.getCause().getCause().getMessage());
            }
        }
    }
}
