package com.example.springapp;

import com.example.springapp.model.User;
import com.example.springapp.repository.UserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Calendar;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringmvcApplicationTests {

	@Autowired
	private UserRepository userRepository;

	@Test
	public void contextLoads() {
	}

	@Test
	public void createUser(){
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

		User user = new User();
		user.setUsername("admin");
		user.setPassword(encoder.encode("admin"));
		user.setInsertDate(Calendar.getInstance());

		userRepository.save(user);
	}

}
