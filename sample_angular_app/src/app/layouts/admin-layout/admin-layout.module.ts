import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AdminLayoutRoutes} from './admin-layout.routing';

import {HomeComponent} from "../../home/home.component";
import {CreatePostComponent} from '../../create-post/create-post.component';
import {PostFormComponent} from '../../post-form/post-form.component';
import {PostDetailsComponent} from '../../post-details/post-details.component';
import {PostsListComponent} from '../../posts-list/posts-list.component';
import {SearchPostsComponent} from '../../search-posts/search-posts.component';
import {UpdatePostComponent} from '../../update-post/update-post.component';
import {MessageComponent} from "../../message/message.component";

import {
    MatButtonModule,
    MatInputModule,
    MatRippleModule,
    MatFormFieldModule,
    MatTooltipModule,
    MatSelectModule
} from '@angular/material';


@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(AdminLayoutRoutes),
        FormsModule,
        ReactiveFormsModule,
        MatButtonModule,
        MatRippleModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatTooltipModule,
    ],
    declarations: [
        HomeComponent,
        CreatePostComponent,
        PostFormComponent,
        PostDetailsComponent,
        PostsListComponent,
        SearchPostsComponent,
        UpdatePostComponent,
        MessageComponent,
    ],
})

export class AdminLayoutModule {
}
