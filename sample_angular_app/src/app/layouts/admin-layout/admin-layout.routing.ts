import {Routes} from '@angular/router';

import {PostsListComponent} from "../../posts-list/posts-list.component";
import {CreatePostComponent} from "../../create-post/create-post.component";
import {UpdatePostComponent} from "../../update-post/update-post.component";
import {SearchPostsComponent} from "../../search-posts/search-posts.component";
import {HomeComponent} from "../../home/home.component";

export const AdminLayoutRoutes: Routes = [
    {path: 'home', component: HomeComponent},
    {path: 'posts', component: PostsListComponent},
    {path: 'create-post', component: CreatePostComponent},
    {path: 'update-post', component: UpdatePostComponent},
    {path: 'search-posts', component: SearchPostsComponent},
];
