import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

import {Post} from "../post";
import {PostService} from "../services/post.service";
import {MessageService} from "../services/message.service";

@Component({
    selector: 'app-search-posts',
    templateUrl: './search-posts.component.html',
    styleUrls: ['./search-posts.component.scss']
})
export class SearchPostsComponent implements OnInit {

    private searchForm: FormGroup;
    private posts: Post[];

    constructor(private postService: PostService, private messageService: MessageService, private formBuilder: FormBuilder) {
    }

    ngOnInit() {
        this.searchForm = this.formBuilder.group({
            post_id: ['', Validators.required]
        });
    }

    get form() { return this.searchForm.controls; }

    searchPost() {
        this.messageService.clearMessage();

        if (this.searchForm.invalid) {
            this.messageService.warning('WARNING: Please enter post id before proceeding.');
            return;
        }
        this.postService.getPost(this.form.post_id.value).subscribe((data) => {
            this.posts = [];
            if (data.code == this.postService.operation_code_success) {
                this.posts.push(data.item);
            } else if (data.code == this.postService.operation_code_error) {
                this.messageService.error('ERROR: ' + data.message);
            } else if (data.code == this.postService.operation_code_warning) {
                this.messageService.warning('WARNING: ' + data.message);
            } else {
                this.messageService.error('An error occurred while processing your request!');
            }
        }, (error) => {
            this.messageService.error('An error occurred while processing your request!');
        });
    }
}
