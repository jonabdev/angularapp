export class User {
  id: number;
  username: string;
  password: string;
  insertDate: object;
  updateDate: object;
}