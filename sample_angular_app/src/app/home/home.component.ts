import {Component, OnInit} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {AuthenticationService} from "../services/authentication.service";
import {MessageService} from "../services/message.service";

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {


    private username: string;

    constructor(private http: HttpClient, private authenticationService: AuthenticationService, private messageService: MessageService,) {
    }

    ngOnInit() {
        this.authenticationService.getAuthenticatedUser().subscribe(user => {
                this.username = user['name'];
            },
            (error) => {
                this.messageService.error('ERROR: An error occurred while processing your request!');
            }
        );
    }
}
