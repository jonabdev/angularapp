import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";

import {PostService} from '../services/post.service';
import {Post} from '../post';
import {MessageService} from "../services/message.service";
import {ConfirmDialogService} from "../services/confirm-dialog.service";

@Component({
    selector: 'app-posts-list',
    templateUrl: './posts-list.component.html',
    styleUrls: ['./posts-list.component.scss']
})

export class PostsListComponent implements OnInit {

    private posts: Post[];
    date_posts: Date = new Date();

    constructor(private postService: PostService, private messageService: MessageService, private confirmDialogService: ConfirmDialogService, private router: Router) {
    }

    ngOnInit() {
        this.postService.getAllPosts().subscribe(data => {
            if (data.code === this.postService.operation_code_success) {

                this.posts = data.item;
                this.checkPostsListEmpty();

            } else if(data.code === this.postService.operation_code_error){
                this.messageService.error('ERROR: ' + data.message);
            } else if(data.code === this.postService.operation_code_warning){
                this.messageService.error('WARNING: ' + data.message);
            }else{
                this.messageService.error('An error occurred while processing your request!');
            }
        }, (error) => {
            this.messageService.error('ERROR: An error occurred while processing your request!');
        });
    }

    openConfirmDialog(post) {
        this.confirmDialogService.confirmAction('Delete Post', 'Are you sure you want to permanently delete this post?')
            .then((confirmed) => this.deletePost(post))
            .catch(() => console.log('User dismissed the dialog'));
    }

    deletePost(post) {
        this.messageService.clearMessage();

        this.postService.deletePost(post.id).subscribe((data) => {
            if (data.code === this.postService.operation_code_success) {

                this.posts.splice(this.posts.indexOf(post), 1);
                this.checkPostsListEmpty();

            } else if(data.code === this.postService.operation_code_error){
                this.messageService.error('ERROR: ' + data.message);
            } else if(data.code === this.postService.operation_code_warning){
                this.messageService.warning('WARNING: ' + data.message);
            }else{
                this.messageService.error('An error occurred while processing your request!');
            }
        }, (error) => {
            this.messageService.error('ERROR: An error occurred while processing your request!');
        });
    }

    updatePost(post) {
        this.postService.setPostObject(post);
        this.router.navigate(['/adm/update-post']);
    }

    private checkPostsListEmpty(){
        if (this.posts.length === 0) {
            this.messageService.warning('There are no posts to show!');
        }
    }
}
