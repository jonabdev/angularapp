import {Component, OnInit} from '@angular/core';
import {AuthenticationService} from "../services/authentication.service";
import {User} from "../user";
import {Router} from "@angular/router";

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit {
    private errorMessage: string;
    private user: User;

    constructor(private authenticationService: AuthenticationService, private router: Router) {
    }

    ngOnInit() {
        this.user = new User();
        localStorage.setItem('isLoggedIn', "false");
        sessionStorage.removeItem('token');
    }

    onSubmit() {
        this.authenticationService.authenticate(this.user).subscribe(data => {
            if (data.item) {
                localStorage.setItem('isLoggedIn', "true");
                sessionStorage.setItem('token', btoa(this.user.username + ':' + this.user.password));
                setTimeout(() => {
                    this.router.navigate(['adm/home']);
                }, 500);
            } else {
                this.errorMessage = data.message;
            }
        });
    }
}
