import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {ActivatedRoute, Router} from "@angular/router";
import {Observable} from "rxjs";
import {User} from "../user";

@Injectable({
    providedIn: 'root'
})
export class AuthenticationService {

    private baseUrl = 'http://localhost:8089';
    private headers = new HttpHeaders({'Authorization': 'Basic ' + sessionStorage.getItem('token')});

    constructor(private route: ActivatedRoute, private router: Router, private http: HttpClient) {
    }

    authenticate(user: User): Observable<any> {
        return this.http.post(`${this.baseUrl}/login`, user);
    }

    getAuthenticatedUser(): Observable<any> {
        return this.http.post<Observable<Object>>( `${this.baseUrl}/user`, {}, {headers: this.headers});
    }

    isAuthenticated(): boolean {
        let token = sessionStorage.getItem('token');
        return (token!=undefined && token !=null &&  token !='');
    }
}
