import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {Post} from "../post";

@Injectable({
    providedIn: 'root'
})
export class PostService {

    private baseUrl = 'http://localhost:8089/ddd/posts';
    private headers = new HttpHeaders({'Authorization': 'Basic ' + sessionStorage.getItem('token')});
    private post: Post;

    public operation_code_success: number = 0;
    public operation_code_error: number = 1;
    public operation_code_warning: number = 2;

    constructor(private http: HttpClient) {
    }

    getAllPosts(): Observable<any> {
        return this.http.get(`${this.baseUrl}`,{headers:this.headers});
    }

    getPost(id: string): Observable<any> {
        return this.http.get(`${this.baseUrl}/${id}`,{headers:this.headers});
    }

    createNewPost(post: Object): Observable<any> {
        return this.http.post(`${this.baseUrl}`, post,{headers:this.headers});
    }

    updatePost(value: any): Observable<any> {
        return this.http.put(`${this.baseUrl}`, value, {headers:this.headers});
    }

    deletePost(id: number): Observable<any> {
        return this.http.delete(`${this.baseUrl}/${id}`, {headers:this.headers, responseType: 'json'});
    }

    setPostObject(post: Post) {
        this.post = post;
    }

    getPostObject() {
        return this.post;
    }
}
