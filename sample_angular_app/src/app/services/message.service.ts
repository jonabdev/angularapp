import {Injectable} from '@angular/core';
import {Observable, Subject} from "rxjs";

@Injectable({
    providedIn: 'root'
})
export class MessageService {

    private subject = new Subject<any>();
    constructor() {
    }

    success(message: string) {
        this.subject.next({type: 'alert alert-success', text: message});
    }

    error(message: string) {
        this.subject.next({type: 'alert alert-danger', text: message});
    }

    warning(message: string) {
        this.subject.next({type: 'alert alert-warning', text: message});
    }

    getMessage(): Observable<any> {
        return this.subject.asObservable();
    }

    clearMessage() {
        this.subject.next();
    }
}