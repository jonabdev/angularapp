import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from "rxjs";
import {MessageService} from "../services/message.service";

@Component({
    selector: 'app-message',
    templateUrl: './message.component.html',
    styleUrls: ['./message.component.scss']
})
export class MessageComponent implements OnInit {

    private subscription: Subscription;
    private message: any = {};

    constructor(private messageService: MessageService) {
    }

    ngOnInit() {
        this.subscription = this.messageService.getMessage().subscribe(message => {
            this.message = message;
        });
    }
}
