export class Post {
  id: number;
  title: string;
  text: string;
  insertDate: object;
  updateDate: object;
}