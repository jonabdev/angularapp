import {Component, OnInit} from '@angular/core';

import {Post} from '../post';
import {PostService} from '../services/post.service';

@Component({
    selector: 'app-create-post',
    templateUrl: './create-post.component.html',
    styleUrls: ['./create-post.component.scss']
})
export class CreatePostComponent implements OnInit {

    private post: Post;

    constructor(private postService: PostService) {
    }

    ngOnInit() {
        this.post = new Post();
        this.postService.setPostObject(this.post);
    }
}


