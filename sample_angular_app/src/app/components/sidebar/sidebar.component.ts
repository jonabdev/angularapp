import {Component, OnInit} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";
import "rxjs-compat/add/operator/finally";

declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}

export const ROUTES: RouteInfo[] = [
    {path: 'home', title: 'Dashboard', icon: 'dashboard', class: ''},
    {path: 'posts', title: 'Posts List', icon: 'list_alt', class: ''},
    {path: 'create-post', title: 'Create Post', icon: 'content_paste', class: ''},
    {path: 'search-posts', title: 'Search Posts', icon: 'search', class: ''},
];

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
    menuItems: any[];

    constructor(private http: HttpClient,  private router: Router) {
    }

    ngOnInit() {
        this.menuItems = ROUTES.filter(menuItem => menuItem);
    }

}
