import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {HttpClientModule} from '@angular/common/http';
import {RouterModule} from '@angular/router';

import {AppRoutingModule} from './app.routing';
import {ComponentsModule} from './components/components.module';

import {AppComponent} from './app.component';
import {LoginComponent} from "./login/login.component";
import {AdminLayoutComponent} from './layouts/admin-layout/admin-layout.component';
import {ConfirmDialogComponent} from "./confirm-dialog/confirm-dialog.component";
import {AuthGuardService as AuthGuard} from "./services/authguard.service";

import {
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatRippleModule,
    MatSelectModule,
    MatTooltipModule
} from "@angular/material";

@NgModule({
    imports: [
        BrowserAnimationsModule,
        FormsModule,
        ReactiveFormsModule,
        MatButtonModule,
        MatRippleModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatTooltipModule,
        NgbModule.forRoot(),
        HttpClientModule,
        ComponentsModule,
        RouterModule,
        AppRoutingModule,
    ],
    declarations: [
        AppComponent,
        LoginComponent,
        AdminLayoutComponent,
        ConfirmDialogComponent,
    ],
    providers: [AuthGuard],
    bootstrap: [AppComponent],
    entryComponents: [ConfirmDialogComponent],
})

export class AppModule {
}

