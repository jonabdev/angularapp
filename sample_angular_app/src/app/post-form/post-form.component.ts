import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";

import {Post} from "../post";
import {PostService} from "../services/post.service";
import {MessageService} from "../services/message.service";

@Component({
    selector: 'app-post-form',
    templateUrl: './post-form.component.html',
    styleUrls: ['./post-form.component.scss']
})
export class PostFormComponent implements OnInit {

    private post: Post;

    constructor(private postService: PostService, private messageService: MessageService, private router: Router) {
    }

    ngOnInit() {
        this.post = this.postService.getPostObject();
    }

    processForm() {
        this.messageService.clearMessage();
        // add new post
        if (this.post.id == undefined) {
            this.postService.createNewPost(this.post).subscribe((data) => {
                if (data.code == this.postService.operation_code_success) {
                    //reset post object
                    this.post = new Post();
                    this.messageService.success('SUCCESS: ' + data.message);

                    setTimeout(() => {
                        this.messageService.clearMessage();
                    }, 1500);

                } else if (data.code == this.postService.operation_code_error) {
                    this.messageService.error('ERROR: ' + data.message);
                } else if (data.code == this.postService.operation_code_warning) {
                    this.messageService.warning('WARNING: ' + data.message);
                } else {
                    this.messageService.error('ERROR: An error occurred while processing your request!');
                }
            }, (error) => {
                this.messageService.error('ERROR: An error occurred while processing your request!');
            });
            // edit post
        } else {
            this.postService.updatePost(this.post).subscribe((data) => {
                if (data.code == this.postService.operation_code_success) {

                    this.messageService.success('SUCCESS: ' + data.message);
                    setTimeout(() => {
                        this.router.navigate(['/adm/posts']);
                    }, 1500);

                } else if (data.code == this.postService.operation_code_error) {
                    this.messageService.error('ERROR: ' + data.message);
                } else if (data.code == this.postService.operation_code_warning) {
                    this.messageService.warning('WARNING: ' + data.message);
                } else {
                    this.messageService.error('An error occurred while processing your request!');
                }
            }, (error) => {
                this.messageService.error('ERROR: An error occurred while processing your request!');
            });
        }
    }
}
